Documentation system
--------------------------------
This documentation serves as the "root" of documentation related to DES Labs. Ideally all relevant docs will be linked from the tree of linked documentation sources starting here.

The documentation is powered by Sphinx, using the reStructuredText (reST) format. With Sphinx installed locally, you can build the HTML files using ::

  cd /docs/
  make html

Then open ``_build/html/index.html`` in your browser.

The ``/.gitlab-ci.yml`` file directs GitLab CI/CD system to automatically build and deploy these docs to https://des-labs.gitlab.io/kubernetes/.
