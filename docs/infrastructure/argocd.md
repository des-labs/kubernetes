ArgoCD
==========================

Installation
-------------------------------------

Install to cluster-dev or cluster-prod using the command below:
```
helm upgrade --install -n default argo-cd charts/argo-cd -f charts/argo-cd/values.yaml -f charts/argo-cd/values-{prod,dev}.yaml 
```

Authentication
-------------------------------------

Following [this documentation](https://argoproj.github.io/argo-cd/operator-manual/user-management/#dex), GitLab has been added as an identity provider for authentication to the ArgoCD sevice. The GitLab Oauth application definition was created through the admin's personal GitLab account. 

Members of the `des-labs` GitLab group can use the "Log in via GitLab" button. 

App structure
-------------------------------------

The goal is to capture the complete state of the deployed services in a reproducible and revertable way by following the GitOps methodology. The combination of ArgoCD and a deployment git repo allows in most cases for apps to be developed and deployed without requiring `kubectl` access to the Kubernetes cluster directly.

There are infrastructure level deployments, including

- ingress controllers
- ArgoCD itself

that are either deployed manually via Helm (ingress controllers) or require manual intervention to start. All other deployments and resources should be under ArgoCD control via the git repo.

There are two "root" apps that follow the "[app of apps](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#app-of-apps)" pattern by creating a parent "root" app that includes separately defined apps in a hierarchical way. These root apps are `apps/cluster-dev` for cluster-dev and `apps/cluster-prod` for cluster-prod. The root app for a cluster is installed by a `des-labs` group user so that it falls under the `des-labs` ArgoCD project. 


Command line access
-------------------------------------

To use the local [ArgoCD CLI](https://argoproj.github.io/argo-cd/getting_started/#2-download-argo-cd-cli), use the more complex login command, which will open a browser window to complete the GitHub authentication:

```
argocd login deslabs.ncsa.illinois.edu --grpc-web-root-path /argo-cd --sso
```

Issues deleting root apps
--------------------------------------

There is a problem when deleting root apps where ArgoCD will get stuck in a neverending "Deleting..." status. In order for ArgoCD to delete apps, it needs to see the finalizer in the app manifest:
```
metadata:
  finalizers:
  - resources-finalizer.argocd.argoproj.io
```

You may need to manually edit the manifest to allow the resource to be deleted:
```
kubectl edit -n your-namespace applications.argoproj.io your-app-name
```

See https://github.com/argoproj/argo-cd/issues/4369 for details.
