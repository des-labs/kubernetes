Building images
===============================

Docker images are built for each of the deployed services typically based on Dockerfiles residing in their respective source code repos.