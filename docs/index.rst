DES Labs Documentation
======================================================

The `DES Labs team <https://deslabs.ncsa.illinois.edu/about.html>`_ is part of the `Dark Energy Survey Data Management team at NCSA <https://des.ncsa.illinois.edu/about>`_. The purpose of DES Labs is to develop and deploy tools and services of use to the scientific community.

.. toctree::
  :maxdepth: 1
  :caption: Infrastructure
  :glob:

  infrastructure/*

.. toctree::
  :maxdepth: 1
  :caption: Apps
  :glob:

  apps/*

Administration and Access
-------------------------------------

The administrators of the Kubernetes clusters are members of the DES Labs team. Access to deployments and secrets originates from 

- access to the Kubernetes cluster control-plane nodes host OS via SSH, and
- membership in the `DES Labs GitLab group <https://gitlab.com/groups/des-labs/-/group_members>`_.

The kubeconfig that provides admin access to Kubernetes resources is found at the standard location ``/etc/kubernetes/admin.conf`` on the control-plane node. This provides ``kubectl`` access to all cluster resources.

GitLab authentication is used to provide access to the ArgoCD instances and `the deployment repo <https://gitlab.com/des-labs/kubernetes>`_ driving the deployment of services.

Kubernetes cluster nodes
-------------------------------------


The development cluster ``cluster-dev`` consists of three nodes::

  $ kubectl get node -o wide
  NAME                              STATUS   ROLES                  AGE    VERSION   INTERNAL-IP      EXTERNAL-IP   OS-IMAGE                KERNEL-VERSION                CONTAINER-RUNTIME
  desapps.cosmology.illinois.edu    Ready    control-plane,master   273d   v1.23.1   141.142.161.36   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12
  desapps2.cosmology.illinois.edu   Ready    <none>                 273d   v1.23.1   141.142.161.53   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12
  desmaster.ncsa.illinois.edu       Ready    <none>                 259d   v1.23.1   141.142.161.55   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12


The production cluster ``cluster-prod`` consists of eight nodes::

  $ kubectl get node -o wide
  NAME                                STATUS   ROLES                  AGE    VERSION   INTERNAL-IP      EXTERNAL-IP   OS-IMAGE                KERNEL-VERSION                CONTAINER-RUNTIME
  *descut3.ncsa.illinois.edu          Ready    control-plane,master   264d   v1.23.1   141.142.161.48   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12
  *descut2.cosmology.illinois.edu     Ready    <none>                 155d   v1.23.1   141.142.161.54   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12
  descut.cosmology.illinois.edu       Ready    <none>                 260d   v1.23.1   141.142.161.46   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12
  descut4.cosmology.illinois.edu      Ready    <none>                 260d   v1.23.1   141.142.161.56   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12
  descut5.cosmology.illinois.edu      Ready    <none>                 264d   v1.23.1   141.142.161.57   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12
  descut6.cosmology.illinois.edu      Ready    <none>                 264d   v1.23.1   141.142.161.58   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12
  desgpu1.cosmology.illinois.edu      Ready    <none>                 263d   v1.23.1   141.142.161.18   <none>        CentOS Linux 7 (Core)   3.10.0-1127.13.1.el7.x86_64   docker://20.10.12
  desrelease.cosmology.illinois.edu   Ready    <none>                 260d   v1.23.1   141.142.161.51   <none>        CentOS Linux 7 (Core)   3.10.0-1160.49.1.el7.x86_64   docker://20.10.12

\* These two nodes run the two ingress controllers associated with the domains ``des.ncsa.illinois.edu`` and ``deslabs.ncsa.illinois.edu``, respectively. There is no floating IP with a load balancer to implement high-availability (HA) services; if either of these nodes are down, the corresponding web services go offline.
