# DES Data Release File Server

The [DES data release file server](http://desdr-server.ncsa.illinois.edu) is a basic NGINX webserver instance that serves official DES data release files for both [the public](http://desdr-server.ncsa.illinois.edu/despublic/) and [the collaboration](http://desdr-server.ncsa.illinois.edu/desprivate/) (access controlled). 

The access control for the collaboration data is implemented by the Kubernetes ingress controller using [Traefik BasicAuth middleware](https://doc.traefik.io/traefik/v2.0/middlewares/basicauth/) with a static username and password.
