# Fluent-bit

We use [Fluent Bit](https://fluentbit.io/) deployed as a daemonset to ship kubernetes container logs (/var/log/containers/*.log) to AWS S3. 

## Configuration

Fluent Bit is configured to upload small files often. This minimizes the memory footprint and potential for data loss, and depends on no persistent storage. Small files may not be the most convenient for frequent log examination. A possible update to the configuration would be to increase the file sizes uploaded to S3. This would require a persistent disk to avoid data loss as files are written/stored before sending.

## S3 

### Access

To access the S3 bucket, click "sign-in" on the [AWS console](https://aws.amazon.com/) and login as an IAM user. The account alias, username and password are found in the secret "fluentbit-s3-iam-access".

Once logged in navigate to the s3 tab (located under the "Services" icon on the top left). You should see the bucket "deslabs-fluent-bit". Once there, you will find the directory structure for the logs currently as:

{cluster}/{pod_name}/{year}/{month}/{day}/{timestamp}-{UUID}.log

### Storage policy

By default objects uploaded to this bucket are tagged with "storagePolicy: default". This is done using an AWS Lambda function. After 30 days any object still tagged as default will be deleted. If any logs want to be kept, you can update the object tag to "storagePolicy: keep" (or any value other than default). This can be done by clicking on the object, and scrolling down to the "Tags" section.

### Querying objects

To view the log without downloading it, click on the desired log and then select under "Object actions" in the top right, "Query with S3 Select". Make sure the format is marked "JSON" and then scroll down to the "SQL Query" section. The following query selects all lines from the log:

```SELECT * FROM s3object s```


## Metrics

To access the DESLabs dashboard, navigate to CloudWatch and click on "Dashboards" on the left panel. You will see a dashboard "DESLabs" where you can track how many objects are in the S3 bucket as well as the total storage size. Additional metrics regarding the Lambda function that auto-tags S3 objects can also be found.

## Costs

Over one month of testing, between 5-10 GB of logs were stored in a bucket. This corresponds to between $5-10 a month in storage.


## Setup

### Create S3 Bucket

- Navigate to S3 from the AWS console
- Click Create bucket
- Give it a name, e.g., "deslabs-fluent-bit" and click "Create Bucket"
- This bucket name will be used in the fluent-bit values.yaml file to configure where the logs should be output:
```
...
[OUTPUT]
name s3
...
bucket <bucket name>
```

### Create CloudWatch Dashboard

- From the AWS console click on CloudWatch
- Click on "Dashboards" and click "Create dashboard" giving it a name, e.g., DESLabs
- Click Add widget --> Line --> Metrics --> S3
- Add the available metrics 
- To add Lambda metrics refer to the end of the Lambda section below

### Create User Group
Log into AWS console and navigate to the IAM dashboard. 
- On the left panel click on "User groups"
- Click "Create group"
- Name the group, e.g., deslabs
- Click "Create policy"

The above user group and the following cloudwatch policy is OPTIONAL. At a minimum what is needed is a user with S3 access.

There are two policies you will add to give access to the DESlabs CloudWatch dashboard and a single S3 bucket:
- Call the first policy "cloudwatch-dashboard" with the following JSON. Make sure to update the <ACCOUNT_ID> with the correct ID and <DASHBOARD_NAME>:
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "cloudwatch:GetDashboard",
            "Resource": "arn:aws:cloudwatch::<ACCOUNT_ID>:dashboard/<DASHBOARD_NAME>"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "cloudwatch:ListDashboards",
            "Resource": "*"
        }
    ]
}
```
- Then Call the second policy "deslabs-one-bucket" with the following JSON:
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::<DASHBOARD_NAME>",
                "arn:aws:s3:::<DASHBOARD_NAME>/*"
            ]
        }
    ]
}
```
- Save the User group
  
### Create IAM User

- Navigate to the IAM dashboard from the AWS console 
- On the left panel click on Users
- Click Add Users
- Type in a name
- Check both boxes for console access and programmatic access
- Take note of the console password (either created or automatically generated). Update the username, password and alias (account ID under which IAM user is created) in the fluentbit-s3-iam-access secret
- Take note of the access key and value. Update the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in the fluentbit-s3-key secret
- the proggrammtic keys will be used in the fluent-bit-s3-access secrets
- Move on to permissions
  - access to S3 and CloudWatch


### Setup deletion policy
- Navigate to the S3 service from the AWS console
- Click on your bucket
- Under "Management" click "Create lifecycle rule"
- Click "Add tag"
  - key: storagePolicy
  - value: default
- Under Lifecycle Rule Action, click "Expire Current Versions of Objects"
- Under "Days after object creation" add 30
- Click save



### Set-up Lambda Function for Automatic Tagging

- Navigate to Lambda service from the AWS console
- Click "Create Function"
- Add Function name, e.g., "tagS3Objects"
- Select "Python3.9" for the Runtime
- Click "Ceate function"
- Navigate to function and click "Create trigger"
- Choose S3 and select the S3 bucket
- Keep Event type for "All object create events"
- Click create trigger
- Navigate back to your newly created function
- In the "Function Code" section paste the following python:

```
import json
import urllib.parse
import boto3
import datetime

print('Loading function')

s3 = boto3.client('s3')
now = datetime.datetime.now()
tagValue = "default"
tagName = "storagePolicy"


def lambda_handler(event, context):
    print("Recevied event: " + json.dumps(event, indent=2))

    #Get the object from the event
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    try:
        response = s3.put_object_tagging(
            Bucket = bucket,
            Key = key,
            Tagging={
                'TagSet': [
                    {
                        'Key': tagName,
                        'Value': tagValue
                    },
                ]
            }
        )
        print('Succesfully tagged {} to {}.'.format(tagName, key))
    except Exception as e:
        print(e)
        print('Error applying tag {} to {}.'.format(tagName, key))
        raise e

```
- This code tags every object uploaded with tag storagePolicy: default
- To add Lambda metrics navigate to your function and click on "Monitor"
- Click "Add to dashboard" and select your newly created dashboard from the list


## Example manual tagging

Below is an example bash script that uses the AWS CLI to tag objects (storagePolicy: keep) with a particular prefix (in this instance production weave logs). Updating the tag will ensure that the particular tagged objects are not automatically deleted given the lifecycle policy of the bucket (deletes storagePolicy:default older than 30 days).

```
#!/bin/bash
# need to install the aws cli: https://docs.aws.amazon.com/cli/v1/userguide/cli-chap-welcome.html

export AWS_ACCESS_KEY_ID=<ACCESS_KEY>
export AWS_SECRET_ACCESS_KEY=<SECRET_ACCESS_KEY>
export AWS_DEFAULT_REGION=<REGION>

bucket="deslabs-fluent-bit"
prefix="prod/weave-net"

now=`date`
echo "Starting tagging: $now"

aws s3api list-objects --bucket $bucket --query 'Contents[].{Key:Key}' --prefix $prefix --output text | xargs -n 1 aws s3api put-object-tagging  --bucket $bucket --tagging 'TagSet=[{Key=storagePolicy,Value=keep}]' --key

end=`date`
echo "Tagging complete: $end"
```