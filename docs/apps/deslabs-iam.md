# DES Labs Identity and Access Management service

We developed and operate an OpenID Connect (OIDC) server (https://deslabs.ncsa.illinois.edu/iam) based on the Django framework and an existing OIDC provider Django app. It uses the private DES collaboration Oracle database to authenticate users on behalf of third-party, OIDC-compatible applications, such as HedgeDoc and Nextcloud.
