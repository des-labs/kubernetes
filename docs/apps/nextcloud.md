# Nextcloud

We operate [a Nextcloud server](https://cloud.des.ncsa.illinois.edu) for use by the DES collaboration. Researchers compiling the data files for an official data release, for example, can use Nextcloud to easily upload files via the web interface or via the installable sync clients that can handle files of any size. The Nextcloud data storage volume resides on the same GPFS filesystem that is mounted to the data release webservers, allowing DESDM team members to efficiently move files into place once they are vetted by the collaboration.

The primary authentication option for our Nextcloud server is the [DES Labs IAM service](https://deslabs.ncsa.illinois.edu/iam/) which uses the private collaboration Oracle database as the identity provider. Any collaborator who authenticates via this option is granted access to Nextcloud automatically.

We also support many more identity providers via the Keycloak+CILogon service. If someone authenticates via Keycloak+CILogon, a DES Labs admin must manually add the new user to one of the authorized access control groups like `des-collaborator` before Nextcloud will provision them a new Nextcloud account.
