{{- if .Values.desaccesspublic.enabled }}
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: desaccess-public
  namespace: argo-cd
spec:
  project: des-labs
  destination:
    server: https://kubernetes.default.svc
    namespace: desaccess-public
  source:
    path: charts/desaccess
    repoURL: https://gitlab.com/des-labs/kubernetes.git
    targetRevision: main
    helm:
      # Extra parameters to set (same as setting through values.yaml, but these take precedence)
      parameters:
      - name: "replicas"
        value: "1"
      - name: "frontend.image.repository"
        value: "registry.gitlab.com/des-labs/kubernetes/desaccess-frontend"
      - name: "frontend.image.tag"
        value: "v2"
      - name: "backend.image.repository"
        value: "registry.gitlab.com/des-labs/kubernetes/desaccess-backend"
      - name: "backend.image.tag"
        value: "v16"
      - name: "docs.image.tag"
        value: "latest"
      - name: "jlab.image.tag"
        value: "latest"
      - name: "task.cutout.image.repository"
        value: "registry.gitlab.com/des-labs/kubernetes/des-task-cutout"
      - name: "task.cutout.image.tag"
        value: "v6"
      - name: "task.query.image.repository"
        value: "registry.gitlab.com/des-labs/kubernetes/des-task-query"
      - name: "task.query.image.tag"
        value: "v5"
      - name: "job.uid"
        value: {{ .Values.desaccess.job.uid | quote }}
      - name: "job.gid"
        value: {{ .Values.desaccess.job.gid | quote }}
      valueFiles:
      - values.yaml
      - values-public.yaml
      {{- with .Values.desaccesspublic.helmValues }}
      values: |
        {{- toYaml . | nindent 8 }}
    {{- end }}
  syncPolicy:
    {{- if .Values.desaccesspublic.sync }}
    automated:
      prune: true
      selfHeal: true
      allowEmpty: false
    {{- end }}
    syncOptions:
      - CreateNamespace=true
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: backups
  namespace: desaccess-public
  labels:
    app.kubernetes.io/name: decentci-backups
    app.kubernetes.io/component: config
data:
  global: |
    enabled: true
    engine: "restic"
    restic:
      server:
        baseurl: "restic-server.deslabs.ncsa.illinois.edu"
    alerts:
      matrix:
        existingSecret: backup-alerts
        urlKey: matrix-url
  backups: |
    - backup-name: decentci-backup-desaccess-public-db
      schedule: "31 7 * * *"
      database:
        type: mysql
        host: desaccess-public-mysql
        auth:
          username: des
          database: des
          existingSecret: desaccess-public-mysql
          passwordKey: db_pass

---
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  annotations:
    sealedsecrets.bitnami.com/cluster-wide: "true"
  creationTimestamp: null
  name: backup-alerts
  namespace: desaccess-public
spec:
  encryptedData:
    matrix-url: AgDHbl2QO6wohTNY86mZtUQ4esoUbEs8cru3TDwpbLPj0xsIBPXAoNdBZlrFxcLhOUSmQJOJNu3k8o6LwcUDQFwokHOD7oaY7pQDTCMnNREDgU4GR+GBGYZLyLhoHsyZ5IW+IMc6gM1qVV2Z2VTbLDrilKvbhtr3gicVVZCpc1+v4gSxIx0lZ3QrpGSV/1kieZeEhz40XjfX7iffbxE4VOjd3WGqx7Tqt7WutRv6YOhtgpUpzETq9Y6Sg2/TqnE2p5TX5x81LgsuP3dlkN6rcgZ8wfXhTIDn4bHWfkc/Ji/FbhQDgypOr3oXA7UehTCuy4TnhSJtJ1mjZMuaTdDCzaCOYNcdbHGR5/VcVAkFKC/fYYVAexpP14QmtjX2Kc5jqiADc3/KlBkMZat04m2t/CbVZe7yYXdAqEDcBjK0WcsgOq0CMNgIjJh73VfahrQ1T1e9n6HLPa/hmEhZhoqoBKygcLbmDHOe/JEY97ic7iFNtcuU6cpkRumBjyifAkMVbO/LoUBVOWJ5B42sOLIW7un2Wuj3PqdeXCiavBkMWV5ez1J6NkZfa6elnSUnDDxbFoir1qGeDo1YTw2V9rDLe2BwfBF5zh+Xl7iuXrU68AXoxnk0uLGe/2apjhZlXNtlL9a0BEPcWVNEPhkBJxoNP18vqcahzUBbNDp2fI2lb5SqpiMjWSq9RuzYzin0XXmXr0axo2LtWmOXCuj5i85K408r3UBwIfzZ6xotFZbx50/tz0qKi4SsywUzcPeSDizKjrY0Lw4hZ7+jS1FGXDkzyBQxcPkQsbRLECp+ZXv/nH0zNK6PQ3DDhJBEsr8KPwI/OuOQ55Wf4fbs6BuOP/w9+uYn71KImZeVvzXpCu2lnt3LsloASjWXQcJ9jYWFEZjxIb9dE/a1cxOfLDWJKNlHCg==
  template:
    metadata:
      annotations:
        sealedsecrets.bitnami.com/cluster-wide: "true"
      creationTimestamp: null
      name: backup-alerts
    type: Opaque
---
apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  annotations:
    sealedsecrets.bitnami.com/cluster-wide: "true"
  creationTimestamp: null
  name: restic-auth
  namespace: desaccess-public
spec:
  encryptedData:
    rest-server-password: AgABSjbVJbAXeszo10R5AaAx7wA4Th+HioGOP+myVNeKnU28lSoO2F4p2vkvcoSEUjffKYFXVRzSN1hAk3dacofXqGwV+zAcelkUvG+uLGerHM7dQ+dCcBiWE+h80O4jNlW1V/kHdZS1FOrmK49GhlUmIoM9ox063c/0oKTf/+yePtK4RTWKiopqyWdtXKcP4mQWdbYqz3olDA4VBajyo1Q3RF/Xgb0GKBZNmXSyW1PGkn4INCIpIg24s3gstdI8XcdLYRwxhIG1DhWTA2yzpEtlqngMjyxjfD8cZO2rxYcwZJNCtWZXncvM0dZqNVitpz+MWtsedSHTZ37Kd2+ScvA8d2vAoL7nHb1IlEvScCUm6Ez603vLOYhjhkXUnLWhWEt59weSqUuYy299NuPk4YPUsEVmBfsN6/wcbEakBoLCWjYLbkIUnXNk34EBCegMhaOsSuw1e1wS9v475SjZFQemlwz7BXFSMoInK5KTNuPpbLxn3E01DNvo8z7gPHX7Zfj9Nzb3agZCN/J5a4zCR1+kasmpajuGiwwFFp8jwwBYRERn0OFdFowOD9pvRJdGyPFF5QuZNEqSSwLpyk5QL8KF5OoDwOi/w4Fuwd4Ein9HYnPauLyVc7ICQDKcXCQ/cns4kPIcwQI/0YYYgsllRSDtmylhJUsj8HizRZF551MvwTd6LgTuWYh7Hjl5e01vMhuFbNCeJA22ICo5HOUNdetcqSPkSBCCwogxzNfZPinNBA==
    rest-server-username: AgDCTuMf/2jbqGSiFBodB6zLn/xByYRa1JmWxKtwK7tX/kRdDHs6uBNECs0XH8X+UtrMBPyWtBbYg+gQuFij2lVW7baorGDzM5yNHZLrKmA59JRrybMxBdu2kTBzzoh/aPMB9JG60SDPzwlNsAMWlIWyx8xEExDmX/tOcr0pdBqyRV1PC6yMjuE66be0z8dBDvIJZm1bpI7MfZwu80pySJxV1huxWBb7uHKIiR1pNjsY27pW8sgHWJoNzg4sE0W8uLi8mq8ONpU95QCoVk03pxXUnkxcOzxyicG4ZxWEVvqd4cbSg9Md5ZI02Ls8Ky04K7giER/C9MSKQ8762uL65/KbBmccrkvT7JMpOWlevl1Nl7hOsOxAZInbpAx7LKvsHCbhDXfNekUeVcj10kDe1Vr0d+D6kt4nLfNqAx8piHbXqpot3bqmsJouy3wBjvOqCc9tJhnqtO2/OFHcTcn+Oj4q2qFR9RazLDiSkIkIvuMS2PXUK+nb5+5X5+69bOSJHjTZCJU4UK7/VUrhvXxS+bktmXW6SVBMxtk48K0HICfwwEoOGWyhqGao8Qy0LNNaVAuYHtS5OMPolM44zN2JEyk/TgY6iWMW/d2iKnh4XbBhkcS5ikA8wUSOlFTpj/Az7hjEXEQcFYc4bT4jjZSZy7D5gqDnEX59Xvj0uwiRwuzB6W+XrVWAUEWcoVzQ3kP34W1MNN5GsA==
    restic-repo-password: AgBA1tQ4FxIqKqHx0IQm74uUswV0FSI9i3y/MrSb2UWuy2fmdAf6dstL5DK5OdeK3S6PIsUeiy7bItU797FHh7itMbzsybG8LvWRUruh15lbc30OKMImcWtzz7q+N03MbzopRr61kj12KSnGJJT43lo35tUyxSYvFWK5mZNHAeA1jSQ7ZhKi+o1J3fFsN5Goum5Kked3QDCVziVH3B0jJBXul3kAJYrHep9gfQA4ntZb0rtic235LkxLRGRpsiBElfXVsUSqmXEk1uaRAaPGJ9Z7Q7x9A6/MR6M3WyzFeceC3QRaogu9JMs/huhX/al5XDIiuiqQvol7eA1EM0DZXx0X/sL7QU8FI+gz5E0jPqeivU5SPVJUloojWO/mLfHfKaeQWwUNGPXbvxSxfTzWsdoeqvZ+LMhzCeQkUVBZdX0Pl5x/1EjRk9oZ6c3192pHtJ83dVNMyTsX6TcG9+7p1B+OoqoWnkbkZ7xK38qCnfu99LVpYcxbOTgmVO67+wwJz5E7bkoGriqWmYKmFXaYP7CzmOzyRDKnkFlNr+wX4G6spv2RyWW4C6EYNkTbPSAAuA2wVNVhiDlDtGezHADk98NlMwxHAEPhIO4HaRyrWgIHqTvguNrELtOeE5M1q2RRkk/50s75Tm1LyKw9Vhn28W+AmRXQZVntZKm9sQVN1DhlL7lPtZf1t2x7s1g0DrZvumRBohVsc2udWoRXe8S0UqUqlY5VugsL7HxSXZ9EgpCBqQvPDjU61Dzk
  template:
    metadata:
      annotations:
        sealedsecrets.bitnami.com/cluster-wide: "true"
      creationTimestamp: null
      name: restic-auth
    type: Opaque
{{- end }}


