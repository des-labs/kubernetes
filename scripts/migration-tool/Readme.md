# Migration tool

This is a prototype that demonstrates how to deploy a container capable of executing code in target containers in the Kubernetes cluster using the Kubernetes API via the Python `kubernetes` library. The Helm chart shows how to create the requisite RBAC objects and a StatefulSet to deploy the container with a repeatable pod name for convenience.
