#!/usr/bin/env python

import os
import sys
import subprocess
from kubernetes import client, config
import logging
from time import sleep
from pod_exec import exec_commands, copy_file_from_pod

# Configure logging
logging.basicConfig(format='%(asctime)s [%(name)-0s] %(levelname)-8s %(message)s')
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

# GLOBAL VARIABLES
kubeconfig = ''


def run_shell_command(command, sleep_time=0.1, env=os.environ):
    log.debug(f'''Running command:\n{command}''')
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False, env=env)
    stdout, stderr = process.communicate()
    if stderr:
        log.error(stderr.decode('utf-8'))
    if stdout:
        log.debug(stdout.decode('utf-8'))
    # log.debug(f'''Sleeping for {sleep_time} seconds...''')
    sleep(sleep_time)
    return stdout.decode('utf-8')


def desaccess_backup_database(namespace='', pod_label_selector='', dump_filedir=''):
    # env = os.environ
    # env['KUBECONFIG'] = kubeconfig
    # log.debug(f'''Backing up DESaccess database "{namespace}":''')
    # pod_name = run_shell_command(command=[
    #     '/bin/bash', '-c',
    #     f'''
    #     kubectl get -n {namespace} pods -l "app={namespace}-mysql" -o=jsonpath='{{.items[0].metadata.name}}' \
    #     '''
    # ], env=env)
    # log.debug(f'''Detected DESaccess database pod: "{pod_name}"''')
    # run_shell_command(command=[
    #     '/bin/bash', '-c',
    #     f'''
    #     kubectl cp -n {namespace} sql_dump.sh {pod_name}:/tmp/sql_dump.sh && \
    #     kubectl exec -n {namespace} {pod_name} -- bash /tmp/sql_dump.sh /tmp/{namespace}.sql && \
    #     kubectl cp -n {namespace} {pod_name}:/tmp/{namespace}.sql /tmp/{namespace}.sql \
    #     '''
    # ], env=env)
    # if not os.path.isfile(f'''/tmp/{namespace}.sql'''):
    #     log.error(f'''Failed to backup {namespace}''')

    if not pod_label_selector:
        pod_label_selector = f'''app={namespace}-mysql'''
    if not dump_filedir:
        dump_filedir = f'''/taiga/{namespace}/db'''
    dump_filename = f'''{namespace}-db.sql'''
    dump_filepath = os.path.join(dump_filedir, dump_filename)
    core_v1 = client.CoreV1Api()
    pods = core_v1.list_namespaced_pod(namespace=namespace, label_selector=pod_label_selector)
    pod_name = pods.items[0].metadata.name
    log.debug(f'''Running mysqldump in pod "{pod_name}"...''')
    exec_commands(core_v1, name = pod_name, namespace=namespace, commands=[
        f'''echo "testing" > /tmp/{dump_filename}''',
        # f'''mysqldump -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE > /tmp/{dump_filename}''',
    ])

    if not os.path.isfile(f'''/tmp/{dump_filename}'''):
        log.error(f'''Failed to backup {namespace}''')
        return
    log.debug(f'''Copying file from pod "{pod_name}"...''')
    copy_file_from_pod(core_v1, pod_name, f'''/tmp/{dump_filename}''', f'''{dump_filedir}''', namespace=namespace)

if __name__ == "__main__":

    try:
        try:
            kubeconfig = sys.argv[1]
            config.load_kube_config(config_file=kubeconfig)
            log.info(f'''Loaded kubeconfig from file "{kubeconfig}.''')
            backup_root_dir='/tmp'
        except:
            log.info("Loading kubeconfig from in-cluster config...")
            config.load_incluster_config()
            backup_root_dir=''
    except:
        log.fatal("Cannot load kubeconfig. Aborting.")
        sys.exit(1)


    core_v1 = client.CoreV1Api()
    for namespace in ['desaccess-private', 'desaccess-public']:
        try:
            desaccess_backup_database(namespace=namespace, dump_filedir=backup_root_dir)
            ## TODO remove break
            break
        except Exception as e:
            log.error(
                f'''Error backing up DESaccess database "{namespace}": {e}''')

    # api_v1 = client.CoreV1Api()
    # nodes = api_v1.list_node()
    # for node in nodes.items:
    #     node_name = node.metadata.name
    #     log.debug(f'''"{node_name}"''')
