from kubernetes.client.rest import ApiException
from kubernetes.stream import stream
from tempfile import TemporaryFile
import tarfile

# Configure logging
import logging
logging.basicConfig(format='%(asctime)s [%(name)-0s] %(levelname)-8s %(message)s')
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def exec_commands(api_instance, name = '', namespace='default', commands=[]):
    
    resp = None
    try:
        resp = api_instance.read_namespaced_pod(name=name, namespace=namespace)
    except ApiException as e:
        if e.status != 404:
            log.debug("Unknown error: %s" % e)
            exit(1)

    # Calling exec and waiting for response
    for command in commands:
        exec_command = [
            '/bin/bash',
            '-c',
            command]
        # When calling a pod with multiple containers running the target container
        # has to be specified with a keyword argument container=<name>.
        resp = stream(api_instance.connect_get_namespaced_pod_exec,
                    name,
                    namespace,
                    command=exec_command,
                    stderr=True, stdin=False,
                    stdout=True, tty=False)
        log.debug("Response: " + resp)

    # # Calling exec interactively
    # exec_command = ['/bin/bash']
    # resp = stream(api_instance.connect_get_namespaced_pod_exec,
    #             name,
    #             namespace,
    #             command=exec_command,
    #             stderr=True, stdin=True,
    #             stdout=True, tty=False,
    #             _preload_content=False)

    # while resp.is_open():
    #     resp.update(timeout=1)
    #     if resp.peek_stdout():
    #         log.debug("STDOUT: %s" % resp.read_stdout())
    #     if resp.peek_stderr():
    #         log.debug("STDERR: %s" % resp.read_stderr())
    #     if commands:
    #         command = commands.pop(0)
    #         log.debug("Running command... %s\n" % command)
    #         resp.write_stdin(command + "\n")
    #     else:
    #         break

    # resp.write_stdin("date\n")
    # sdate = resp.readline_stdout(timeout=3)
    # log.debug("Server date command returns: %s" % sdate)
    # resp.write_stdin("whoami\n")
    # user = resp.readline_stdout(timeout=3)
    # log.debug("Server user is: %s" % user)
    # resp.close()


def copy_file_from_pod(api_instance, pod_name, src_path, dest_path, namespace="default"):

    exec_command = ['tar', 'cf', '-', src_path]

    with TemporaryFile() as tar_buffer:
        resp = stream(api_instance.connect_get_namespaced_pod_exec, pod_name, namespace,
                      command=exec_command,
                      stderr=True, stdin=True,
                      stdout=True, tty=False,
                      _preload_content=False)

        while resp.is_open():
            resp.update(timeout=1)
            if resp.peek_stdout():
                out = resp.read_stdout()
                # print("STDOUT: %s" % len(out))
                tar_buffer.write(out.encode('utf-8'))
            if resp.peek_stderr():
                print("STDERR: %s" % resp.read_stderr())
        resp.close()

        tar_buffer.flush()
        tar_buffer.seek(0)

        with tarfile.open(fileobj=tar_buffer, mode='r:') as tar:
            for member in tar.getmembers():
                if member.isdir():
                    continue
                fname = member.name.rsplit('/', 1)[1]
                tar.makefile(member, dest_path + '/' + fname)
