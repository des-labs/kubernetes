from jinja2 import Template
import os
import json
from pathlib import PurePath

base_dir = './index_files/despublic'

def scan_filepaths(filepaths_list='despublic_index.full.json'):
    files = []
    with open(filepaths_list) as indexfile:
        for line in indexfile:
            files.append(json.loads(line))
    # print(files)
    file_lists = {}
    file_idx = 0
    for file in files:
        file_idx += 1
        filepath = PurePath(file['key'])
        file_size = file['size']
        if not (filepath.parent and str(filepath.parent) not in ['.', '/']):
            continue
        if str(filepath.parent) not in file_lists:
            # Initialize directory file list
            file_lists[str(filepath.parent)] = []
        if str(filepath) not in file_lists[str(filepath.parent)] and str(filepath.name) != 'index.html':
            file_lists[str(filepath.parent)].append({
                'name': str(filepath.name),
                'size': file_size,
            })
        if file_idx % 10000 == 0:
            with open('file_list.json', 'w') as indexfile:
                json.dump(file_lists, indexfile, indent=2)
    with open('file_list.json', 'w') as indexfile:
        json.dump(file_lists, indexfile, indent=2)

def compile_dirs():
    with open('file_list.json') as indexfile:
        file_lists = json.load(indexfile)

    dirs_list = {}
    for dirpath_str in file_lists:
        dirpath = PurePath(dirpath_str)
        while dirpath.parent and str(dirpath.parent) not in ['.', '/']:
            parent_dir = str(dirpath.parent)
            if parent_dir not in dirs_list:
                # print(f'''parent_dir: "{parent_dir}"''')
                dirs_list[parent_dir] = []
            if not [dir for dir in dirs_list[parent_dir] if dir['name'] == f'''{str(dirpath.name)}/''']:
                dirs_list[parent_dir].append({
                    'name': f'''{str(dirpath.name)}/''',
                    'size': 0,
                })
            dirpath = PurePath(dirpath.parent)
            # print(dirs_list)
    with open('dirs_list.json', 'w') as indexfile:
        json.dump(dirs_list, indexfile, indent=2)

def combine_indices():
    with open('file_list.json') as fp:
        files_list = json.load(fp)
    with open('dirs_list.json') as fp:
        dirs_list = json.load(fp)
    all_list = {}
    for idx, val in files_list.items():
        all_list[idx] = val
    for idx, val in dirs_list.items():
        if idx in all_list:
            all_list[idx].extend(val)
        else:
            all_list[idx] = val
    with open('all_list.json', 'w') as fp:
        json.dump(all_list, fp, indent=2)

def generate_index_files(sub_dir='.'):
    root_dir = os.path.join(base_dir, sub_dir)
    with open('all_list.json') as fp:
        all_list = json.load(fp)
    for path, links in all_list.items():
        os.makedirs(os.path.join(root_dir, path), exist_ok=True)
        index_filepath = os.path.join(root_dir, path, 'index.html')
        with open(os.path.join(os.path.dirname(__file__), "index.tpl.html")) as fp:
            templateText = fp.read()
        template = Template(templateText)
        html = template.render(
            path=path,
            links=links,
        )
        with open(index_filepath, 'w') as fp:
            fp.write(html)

def generate_mc_cp_commands(sub_dir='.'):
    with open('mc_cp_commands.sh', 'w') as outfile:
        for dirpath, dirnames, filenames in os.walk(os.path.join(base_dir, sub_dir)):
            for filename in filenames:
                file_path = os.path.join(dirpath, filename)
                if filename == 'index.html':
                    outfile.write(f'''mc cp --quiet --json {file_path} osn-des/phy240006-bucket01/despublic{file_path.split(sep=base_dir)[1]}\n''')

def generate_mc_ls_commands():
    with open('mc_ls_commands.sh', 'w') as outfile:
        for dirpath, dirnames, filenames in os.walk(base_dir):
            for filename in filenames:
                file_path = os.path.join(dirpath, filename)
                if filename == 'index.html':
                    outfile.write(f'''mc ls osn-des/phy240006-bucket01/despublic{file_path.split(sep=base_dir)[1]}\n''')

if __name__ == "__main__":
    '''
    Step 1: Dump a JSON file containing all files in the bucket
        $ mc tree --json osn-des/phy240006-bucket01/despublic > despublic_index.full.json
      or a subpath
        $ mc tree --json osn-des/phy240006-bucket01/despublic/y3a2_files > despublic_index.y3a2_files.json
    '''
    sub_dir='.'
    scan_filepaths(filepaths_list='despublic_index.full.json')

    '''
    Step 2 & 3: Compile file and directory path info into a single data structure
    '''
    compile_dirs()
    combine_indices()

    '''
    Step 4: Generate HTML `index.html` files
    '''
    generate_index_files(sub_dir=sub_dir)

    '''
    Step 5: Verify that there are no existing index.html files to be overwritten
        $ python generate_index_files.py > mc_ls_commands.sh
        $ bash -x mc_ls_commands.sh > mc_ls_results.log 2>&1
        $ wc -l mc_ls_commands.sh 
        20633 mc_ls_commands.sh
        $ wc -l mc_ls_results.log 
        20633 mc_ls_results.log
        $ grep -v -C5 "+ mc" mc_ls_results.log
    '''
    # generate_mc_ls_commands(sub_dir=sub_dir)

    '''
    Step 6: Upload index files to bucket
        $ python generate_index_files.py > mc_cp_commands.sh
        $ bash -x mc_cp_commands.sh >> mc_cp_results.log 2>&1
    '''
    # generate_mc_cp_commands(sub_dir=sub_dir)
