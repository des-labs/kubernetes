# Index file generator for DES Data Release file server

This script will generate `index.html` files for each subdirectory in the `despublic` S3 bucket on OSN. The data files are served at https://desdr-server.ncsa.illinois.edu/despublic/ via an [NGINX S3 gateway](https://github.com/nginxinc/nginx-s3-gateway). This gateway software has a bug that truncates automatic directory listings that exceed some length, which occurs in several directories including the `dr2_tiles` directory. 

Enabling the `PROVIDE_INDEX_PAGE` option will use `index.html` files instead of the automatically generated index, but there appears to be no way to combine `ALLOW_DIRECTORY_LIST` with this option such that directories with an `index.html` file use that file and those without use the automatic index; it is one or the other.
