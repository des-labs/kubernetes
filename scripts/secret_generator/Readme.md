# Sealed Secret Generator

Create a `secrets.yaml` file following the example below:

```yaml
secrets:
- name: argocd-secrets
  data:
    server.secretkey: "*****"
- name: discourse-admin
  data:
    discourse-password: "*****"
```

Run the bulk secrets generator to create Sealed Secret manifests for each secret in `secrets.yaml` that contains a `name` mapping.

```sh
python seal_bulk_secrets.py --file secrets.yaml
```

The generated secrets files are found in `/tmp/sealed-secrets`.
