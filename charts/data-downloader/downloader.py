#!/usr/bin/env python3

import requests
import os
import time
from requests.auth import HTTPBasicAuth
from html.parser import HTMLParser
import logging

# Configure logging
logging.basicConfig(
    format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s',
)
log = logging.getLogger("data-downloader")
try:
    log.setLevel(os.environ['LOG_LEVEL'].upper())
except:
    log.setLevel('INFO')

USERNAME = os.environ['HTTP_AUTH_USERNAME']
PASSWORD = os.environ['HTTP_AUTH_PASSWORD']
BASE_URL = os.environ['BASE_URL']
DATA_DIR = os.environ['DATA_DIR']
DOWNLOAD_DIR = os.path.join(DATA_DIR, 'download')
BASIC_AUTH_HEADER = HTTPBasicAuth(USERNAME, PASSWORD)

class MyHTMLParser(HTMLParser):
    def __init__(self, *, convert_charrefs: bool = ...) -> None:
        super().__init__(convert_charrefs=convert_charrefs)
        self.parse_table = False
        self.parse_size = False
        self.row_index = -1
        self.downloads = {}

    def handle_starttag(self, tag, attrs):
        if tag == 'tbody':
            # Start reading table
            self.parse_table = True
        elif self.parse_table and tag == 'tr':
            # A new row has been discovered
            self.row_index += 1
        elif self.parse_table and tag == 'a' and 'download' in [attr[0] for attr in attrs]:
            download_url = [attr[1] for attr in attrs if attr[0] == 'href'][0]
            # log.info(f'''[{self.row_index}] {download_url}''')
            self.downloads[self.row_index] = {
                'url': download_url,
            }
        elif self.parse_table and tag == 'td' and 'title' in [attr[0] for attr in attrs]:
            download_size = [attr[1] for attr in attrs if attr[0] == 'title'][0]
            # log.info(f'''[{self.row_index}] {download_size}''')
            self.parse_size = True

    def handle_data(self, data):
        if self.parse_size:
            self.downloads[self.row_index]['size'] = int(data)
            self.parse_size = False

    def handle_endtag(self, tag):
        if tag == 'tbody':
            # Stop reading table
            self.parse_table = False

if __name__ == "__main__":
    indexFilePath = os.path.join(DATA_DIR, 'index.html')
    try:
        with open(indexFilePath, "r") as f:
            log.info(f'''Using existing HTML document previously downloaded.''')
            raw_html = f.read()
    except:
        log.info(f'''Downloading HTML index of data files...''')
        response = requests.get(BASE_URL, auth=BASIC_AUTH_HEADER)
        raw_html = response.text
        # log.info(raw_html)
        with open(indexFilePath, 'w') as f:
            f.write(raw_html)
    
    parser = MyHTMLParser()
    parser.feed(raw_html)
    
    number_downloads = len(parser.downloads)
    
    total_size = 0
    for index, data_file in parser.downloads.items():
        total_size += data_file['size']
    log.info(f'''Total size to download: {total_size}''')
    # log.info(f'''Total size to download: {total_size/1024.0/1024.0}''')
        
    log.info(f'''Downloading data files...''')
    total_size_downloaded = 0.0
    avg_download_speed = 0.0
    ## Array of bytes/sec for each download
    download_times = {}
    os.makedirs(DOWNLOAD_DIR, exist_ok=True)
    for index, data_file in parser.downloads.items():
        percent_downloaded = round(100.0*total_size_downloaded/total_size, 1)
        stats = f'''[{index}/{number_downloads} files, {percent_downloaded}% total size, {round(avg_download_speed/1024.0)} KiB/s avg]'''
        file_url = f'''{BASE_URL}/{data_file['url']}'''
        download_file_path = os.path.join(DOWNLOAD_DIR, os.path.basename(file_url))
        if os.path.exists(download_file_path) and os.stat(download_file_path).st_size == data_file['size']:
            total_size_downloaded += data_file['size']
            log.info(f'''{stats} Already downloaded: "{os.path.basename(file_url)}" ({data_file['size']}). Skipping.''')
            continue
        start_download = time.time()
        with requests.get(file_url, auth=BASIC_AUTH_HEADER, stream=True,) as r:
            log.info(f'''{stats} Downloading "{os.path.basename(file_url)}" ({data_file['size']})...''')
            r.raise_for_status()
            with open(download_file_path, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                    f.write(chunk)
        finish_download = time.time()
        download_times[index] = data_file['size']/(finish_download-start_download)
        avg_download_speed = sum([download_times[idx] for idx in download_times])/len(download_times)
        total_size_downloaded += data_file['size']
    log.info(f'''Files downloaded to "{DOWNLOAD_DIR}".''')
