---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-frontend
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-frontend
  strategy: 
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  replicas: {{ .Values.replicas }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-frontend
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - {{ .Release.Name }}-frontend
              topologyKey: "kubernetes.io/hostname"
      imagePullSecrets:
      - name: registry-auth-{{ .Release.Name }}
      volumes:
      - name: srv-volume
        emptyDir: {}
      initContainers:
      - image: {{ .Values.frontend.image.repository }}:{{ .Values.frontend.image.tag }}
        imagePullPolicy: Always
        name: init
        securityContext:
          runAsUser: 0
          runAsGroup: 0
        command: ['/bin/bash', '-c', 'cp -R /srv/* /nodesrv/ && cd /nodesrv && source init.sh && chown -R node:node /nodesrv']
        volumeMounts:
        - name: srv-volume
          mountPath: /nodesrv
        env:
          - name: DESACCESS_INTERFACE
            value: "{{ .Values.desaccessInterface }}"
          - name: NPM_SCRIPT
            value: "{{ .Values.npmBuild }}"
          - name: BACKEND_BASE_URL
            value: "https://{{ .Values.hostname }}"
          - name: FRONTEND_BASE_URL
            value: "https://{{ .Values.hostname }}"
          - name: API_ROOT_PATH
            value: "{{ .Values.apiroot }}"
          - name: WEB_ROOT_PATH
            value: "{{ .Values.webroot }}"
          - name: FILESERVER_ROOT_PATH
            value: "{{ .Values.fileroot }}"
          - name: LIMIT_CUTOUTS_CUTOUTS_PER_JOB
            value: "{{ .Values.limitCutoutsCutoutsPerjob }}"
          - name: TICKET_AUTH
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-desticket-auth
                key: auth
      containers:
      - image: {{ .Values.frontend.image.repository }}:{{ .Values.frontend.image.tag }}
        imagePullPolicy: Always
        name: {{ .Release.Name }}-frontend
        command: ['/bin/bash', '-c', 'cd /nodesrv && bash frontend.entrypoint.sh']
        volumeMounts:
        - name: srv-volume
          mountPath: /nodesrv
        env:
          - name: DESACCESS_INTERFACE
            value: "{{ .Values.desaccessInterface }}"
          - name: NPM_SCRIPT
            value: "{{ .Values.npmBuild }}"
          - name: BACKEND_BASE_URL
            value: "https://{{ .Values.hostname }}"
          - name: FRONTEND_BASE_URL
            value: "https://{{ .Values.hostname }}"
          - name: API_ROOT_PATH
            value: "{{ .Values.apiroot }}"
          - name: WEB_ROOT_PATH
            value: "{{ .Values.webroot }}"
          - name: FILESERVER_ROOT_PATH
            value: "{{ .Values.fileroot }}"
          - name: TICKET_AUTH
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-desticket-auth
                key: auth

---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-backend
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-backend
  strategy:
    type: RollingUpdate
  replicas: {{ .Values.replicas }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-backend
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - {{ .Release.Name }}-backend
              topologyKey: "kubernetes.io/hostname"
      serviceAccountName: jobhandler
      securityContext:
        runAsUser: {{ .Values.job.uid }}
        runAsGroup: {{ .Values.job.gid }}
      imagePullSecrets:
      - name: registry-auth-{{ .Release.Name }}
      containers:
      - image: {{ .Values.backend.image.repository }}:{{ .Values.backend.image.tag }}
        imagePullPolicy: Always
        name: {{ .Release.Name }}-backend
        command: ["python3", "main.py"]
        env:
          # The PersistentVolumeClaim naming convention is to use a common base string
          # and append the task/job type dynamically
          - name: PVC_NAME_BASE
            value: "{{ .Release.Name }}-tasks"
          - name: BASE_PATH
            value: "{{ .Values.apiroot }}"
          - name: BASE_DOMAIN
            value: "{{ .Values.hostname }}"
          # TODO: The TLS secret is unnecessary for Traefik ingress controllers with LetsEncrypt certs
          - name: TLS_SECRET
            value: ""
          - name: FRONTEND_BASE_URL
            value: "https://{{ .Values.hostname }}{{ .Values.webroot }}"
          - name: SERVICE_PORT
            value: "{{ .Values.servicePortBackend }}"
          - name: API_BASE_URL
            value: "http://{{ .Release.Name }}-backend:{{ .Values.servicePortBackend }}{{ .Values.apiroot }}"
          - name: DOCKER_IMAGE_TASK_QUERY
            value: "{{ .Values.task.query.image.repository }}:{{ .Values.task.query.image.tag }}"
          - name: DOCKER_IMAGE_TASK_CUTOUT
            value: "{{ .Values.task.cutout.image.repository }}:{{ .Values.task.cutout.image.tag }}"
          # TODO: Ingress class is unnecessary for Traefik ingress controllers
          - name: INGRESS_CLASS_JLAB_SERVER
            value: "{{ .Values.ingressClass }}"
          - name: DOCKER_IMAGE_JLAB_SERVER
            value: "{{ .Values.jlab.image.repository }}:{{ .Values.jlab.image.tag }}"
          - name: MYSQL_HOST
            value: "{{ .Release.Name }}-mysql"
          - name: MYSQL_DATABASE
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-mysql
                key: db_name
          - name: MYSQL_USER
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-mysql
                key: db_user
          - name: MYSQL_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-mysql
                key: db_pass
          - name: SERVICE_ACCOUNT_DB
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-service-accounts
                key: db
          - name: SERVICE_ACCOUNT_USER
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-service-accounts
                key: user
          - name: SERVICE_ACCOUNT_PASS
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-service-accounts
                key: pass
          - name: HOST_NETWORK
            value: "false"
          - name: JWT_TTL_SECONDS
            value: "3600"
          - name: DROP_TABLES
            value: "{{ .Values.dropTables }}"
          - name: DEBUG_JOB
            value: "{{ .Values.debugJob }}"
          - name: CONFIG_FOLDER_ROOT
            value: "/"
          - name: JWT_HS256_SECRET
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-jwt
                key: jwt_hs256_secret
          # The order of these databases matters. The first database listed is used in cases where a database is not specified.
          - name: ORACLE_USER_MANAGER_DB_PRIVATE_DBS
            value: 'desoper,dessci'
          - name: DESACCESS_INTERFACE
            value: "{{ .Values.desaccessInterface }}"
          - name: SMTP_SERVER
            value: 'smtp-cx.socketlabs.com:587'
          - name: SMTP_USERNAME
            valueFrom:
              secretKeyRef:
                name: smtp
                key: username
          - name: SMTP_PASSWORD
            valueFrom:
              secretKeyRef:
                name: smtp
                key: password
          - name: DESACCESS_ADMIN_EMAILS
            value: 'desaccess-admins@lists.ncsa.illinois.edu'
          - name: DESACCESS_PUBLIC_EMAILS
            value: 'desaccess-public@lists.ncsa.illinois.edu'
          - name: DESACCESS_PRIVATE_EMAILS
            value: 'desaccess-collab@lists.ncsa.illinois.edu'
          - name: ORACLE_USER_MANAGER_DB_PUBLIC_DBS
            value: 'desdr'
          - name: ALLOWED_ROLE_LIST
            value: "{{ .Values.desaccessAllowedRoleList }}"
          - name: JIRA_DEFAULT_ASSIGNEE
            value: "{{ .Values.desaccessJiraDefaultAssignee }}"
          - name: LIMIT_CUTOUTS_CONCURRENT_JOBS
            value: "{{ .Values.limitCutoutsConcurrentJobs }}"
          - name: LIMIT_CUTOUTS_CUTOUTS_PER_JOB
            value: "{{ .Values.limitCutoutsCutoutsPerjob }}"
          - name: LIMIT_MAX_JOB_DURATION_HOURS
            value: "{{ .Values.limitMaxJobDurationHours }}"
          - name: DESACCESS_JOB_FILES_LIFETIME
            value: {{ .Values.desaccessJobFilesLifetime | quote }}
          - name: DESACCESS_JOB_FILES_WARNING_PERIOD
            value: {{ .Values.desaccessJobFilesWarningPeriod | quote }}
          - name: DESACCESS_JOB_FILES_MAX_RENEWALS
            value: "{{ .Values.desaccessJobFilesMaxRenewals }}"
          - name: MONITOR_SERVICE_ACCOUNT_USERNAME
            value: "{{ .Values.monitorAccountServiceName }}"
          - name: DESARCHIVE_HOST_PATH
            value: "{{ .Values.data.desarchive.path }}"
          - name: COADD_HOST_PATH
            value: "{{ .Values.data.coadd.path }}"
          - name: DR2_TILE_HOST_PATH
            value: "{{ .Values.data.dr2_tiles.path }}"
          - name: JOB_UID
            value: "{{ .Values.job.uid }}"
          - name: JOB_GID
            value: "{{ .Values.job.gid }}"
          - name: JOB_RESOURCE_REQUEST_CPU
            value: {{ .Values.job.resources.requests.cpu | quote }}
          - name: JOB_RESOURCE_LIMIT_CPU
            value: {{ .Values.job.resources.limits.cpu | quote }}
          - name: JOB_RESOURCE_REQUEST_MEMORY
            value: {{ .Values.job.resources.requests.memory | quote }}
          - name: JOB_RESOURCE_LIMIT_MEMORY
            value: {{ .Values.job.resources.limits.memory | quote }}
        volumeMounts:
        - name: db-init
          mountPath: "/config"
          # subPath: "db_init.yaml"
        - name: job-files
          mountPath: "/jobfiles"
        - name: jira-access
          mountPath: "/home/worker/jira_access.yaml"
          subPath: "jira_access.yaml"
        - name: oracle-user-manager
          mountPath: "/home/worker/oracle_user_manager.yaml"
          subPath: "oracle_user_manager.yaml"
        - name: desarchive-volume
          mountPath: "/des003/desarchive"
          readOnly: true
        - name: coadd-volume
          mountPath: "/des004/coadd"
          readOnly: true
        - name: dr1-volume
          mountPath: '/tiles/dr1'
          readOnly: true
        - name: dr2-volume
          mountPath: '/tiles/dr2'
          readOnly: true
        - name: email-file-storage
          mountPath: '/email_list'
      volumes:
      - name: db-init
        secret:
          secretName: {{ .Release.Name }}-backend-db-init
          items:
          - key: db_init
            path: "db_init.yaml"
      - name: job-files
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-tasks
      - name: jira-access
        secret:
          secretName: {{ .Release.Name }}-jira
      - name: oracle-user-manager
        secret:
          secretName: {{ .Release.Name }}-oracle-user-manager
      - name: desarchive-volume
        hostPath:
          path: {{ .Values.data.desarchive.path | quote }}
      - name: coadd-volume
        hostPath:
          path: {{ .Values.data.coadd.path | quote }}
      - name: dr1-volume
        hostPath:
          path: {{ .Values.data.dr1_tiles.path | quote }}
      - name: dr2-volume
        hostPath:
          path: {{ .Values.data.dr2_tiles.path | quote }}
      - name: email-file-storage
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-email
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-mysql
  {{- with .Values.database.deployment.annotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-mysql
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-mysql
    spec:
      securityContext:
        runAsUser: 999
        fsGroup: 999
      containers:
      - image: "mariadb@sha256:b3116f425f51353e2ba71b04647977b035c2c578d276e2d4285bd2798e8199ae"
        name: {{ .Release.Name }}-mysql
        env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-mysql
              key: rootpass
        - name: MYSQL_DATABASE
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-mysql
              key: db_name
        - name: MYSQL_USER
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-mysql
              key: db_user
        - name: MYSQL_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-mysql
              key: db_pass
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
      volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-mysql
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-desaccess-docs
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-desaccess-docs
  strategy:
    type: RollingUpdate
  replicas: {{ .Values.replicas }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-desaccess-docs
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - {{ .Release.Name }}-desaccess-docs
              topologyKey: "kubernetes.io/hostname"
      imagePullSecrets:
      - name: registry-auth-{{ .Release.Name }}
      containers:
      - image: "{{ .Values.docs.image.repository }}:{{ .Values.docs.image.tag }}"
        imagePullPolicy: Always
        name: {{ .Release.Name }}-desaccess-docs
        volumeMounts:
        - name: nginx-config
          mountPath: /etc/nginx/conf.d/default.conf
          subPath: default.conf
      volumes:
      - name: nginx-config
        configMap:
          name: {{ .Release.Name }}-desaccess-docs-nginx-config

---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-fileserver
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-fileserver
  strategy:
    type: RollingUpdate
  replicas: {{ .Values.replicas }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-fileserver
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - {{ .Release.Name }}-fileserver
              topologyKey: "kubernetes.io/hostname"
      containers:
      - image: nginx:1.19.0
        name: {{ .Release.Name }}-fileserver
        volumeMounts:
        - mountPath: /usr/share/nginx/html{{ .Values.fileroot }}
          name: file-storage
          readOnly: true
        - name: nginx-config
          mountPath: /etc/nginx/conf.d/default.conf
          subPath: default.conf
      volumes:
      - name: nginx-config
        configMap:
          name: {{ .Release.Name }}-fileserver-nginx-config
      - name: file-storage
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-tasks
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-email-list
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-email-list
  strategy:
    type: RollingUpdate
  replicas: {{ .Values.replicas }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-email-list
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - {{ .Release.Name }}-email-list
              topologyKey: "kubernetes.io/hostname"
      {{- if .Values.volumePermissions.email }}
      initContainers:
      - name: storage-permission-fix
        image: busybox:1.35
        imagePullPolicy: IfNotPresent
        command: ["/bin/chmod","-R","ugo+rwX","/email_list"]
        volumeMounts:
        - name: file-storage
          mountPath: '/email_list'
        securityContext:
          runAsGroup: 0
          runAsUser: 0
          runAsNonRoot: false
      {{- end }}
      containers:
      - image: nginx:1.19.0
        name: {{ .Release.Name }}-email-list
        volumeMounts:
        - mountPath: /usr/share/nginx/html{{ .Values.emaillistroot }}
          name: file-storage
          readOnly: true
        - name: nginx-config
          mountPath: /etc/nginx/conf.d/default.conf
          subPath: default.conf
      volumes:
      - name: nginx-config
        configMap:
          name: {{ .Release.Name }}-email-list-nginx-config
      - name: file-storage
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-email

---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-easyaccess-docs
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-easyaccess-docs
  strategy:
    type: RollingUpdate
  replicas: {{ .Values.replicas }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-easyaccess-docs
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - {{ .Release.Name }}-easyaccess-docs
              topologyKey: "kubernetes.io/hostname"
      imagePullSecrets:
      - name: registry-auth-{{ .Release.Name }}
      containers:
      - image: "{{ .Values.easyaccessDocs.image.repository }}:{{ .Values.easyaccessDocs.image.tag }}"
        imagePullPolicy: Always
        name: {{ .Release.Name }}-easyaccess-docs
        volumeMounts:
        - name: nginx-config
          mountPath: /etc/nginx/conf.d/default.conf
          subPath: default.conf
      volumes:
      - name: nginx-config
        configMap:
          name: {{ .Release.Name }}-easyaccess-docs-nginx-config

{{ if .Values.devMode }}
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-mysql-debugger
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-mysql-debugger
  strategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-mysql-debugger
    spec:
      securityContext:
        runAsUser: 999
        fsGroup: 999
      containers:
      - image: "mariadb@sha256:b3116f425f51353e2ba71b04647977b035c2c578d276e2d4285bd2798e8199ae"
        name: mysql-debugger
        command: ['/bin/sh', '-c', 'sleep 1000d']
        env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-mysql
              key: rootpass
        - name: MYSQL_DATABASE
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-mysql
              key: db_name
        - name: MYSQL_USER
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-mysql
              key: db_user
        - name: MYSQL_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-mysql
              key: db_pass
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
      volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-mysql

---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Release.Name }}-backend-maintenance
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-backend-maintenance
  strategy:
    type: RollingUpdate
  replicas: 1
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-backend-maintenance
    spec:
      serviceAccountName: jobhandler
      securityContext:
        runAsUser: {{ .Values.job.uid }}
        runAsGroup: {{ .Values.job.gid }}
      imagePullSecrets:
      - name: registry-auth-{{ .Release.Name }}
      containers:
      - image: {{ .Values.backend.image.repository }}:{{ .Values.backend.image.tag }}
        imagePullPolicy: Always
        name: maintenance
        command: ['/bin/sh', '-c', 'sleep 1000d']
        env:
          # The PersistentVolumeClaim naming convention is to use a common base string
          # and append the task/job type dynamically
          - name: PVC_NAME_BASE
            value: "{{ .Release.Name }}-tasks"
          - name: BASE_PATH
            value: "{{ .Values.apiroot }}"
          - name: BASE_DOMAIN
            value: "{{ .Values.hostname }}"
          # TODO: The TLS secret is unnecessary for Traefik ingress controllers with LetsEncrypt certs
          - name: TLS_SECRET
            value: ""
          - name: FRONTEND_BASE_URL
            value: "https://{{ .Values.hostname }}{{ .Values.webroot }}"
          - name: SERVICE_PORT
            value: "{{ .Values.servicePortBackend }}"
          - name: API_BASE_URL
            value: "http://{{ .Release.Name }}-backend:{{ .Values.servicePortBackend }}{{ .Values.apiroot }}"
          - name: DOCKER_IMAGE_TASK_QUERY
            value: "{{ .Values.task.query.image.repository }}:{{ .Values.task.query.image.tag }}"
          - name: DOCKER_IMAGE_TASK_CUTOUT
            value: "{{ .Values.task.cutout.image.repository }}:{{ .Values.task.cutout.image.tag }}"
          # TODO: Ingress class is unnecessary for Traefik ingress controllers
          - name: INGRESS_CLASS_JLAB_SERVER
            value: "{{ .Values.ingressClass }}"
          - name: DOCKER_IMAGE_JLAB_SERVER
            value: "{{ .Values.jlab.image.repository }}:{{ .Values.jlab.image.tag }}"
          - name: MYSQL_HOST
            value: "{{ .Release.Name }}-mysql"
          - name: MYSQL_DATABASE
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-mysql
                key: db_name
          - name: MYSQL_USER
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-mysql
                key: db_user
          - name: MYSQL_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-mysql
                key: db_pass
          - name: SERVICE_ACCOUNT_DB
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-service-accounts
                key: db
          - name: SERVICE_ACCOUNT_USER
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-service-accounts
                key: user
          - name: SERVICE_ACCOUNT_PASS
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-service-accounts
                key: pass
          - name: HOST_NETWORK
            value: "false"
          - name: JWT_TTL_SECONDS
            value: "3600"
          - name: DROP_TABLES
            value: "{{ .Values.dropTables }}"
          - name: DEBUG_JOB
            value: "{{ .Values.debugJob }}"
          - name: CONFIG_FOLDER_ROOT
            value: "/"
          - name: JWT_HS256_SECRET
            valueFrom:
              secretKeyRef:
                name: {{ .Release.Name }}-jwt
                key: jwt_hs256_secret
          # The order of these databases matters. The first database listed is used in cases where a database is not specified.
          - name: ORACLE_USER_MANAGER_DB_PRIVATE_DBS
            value: 'desoper,dessci'
          - name: DESACCESS_INTERFACE
            value: "{{ .Values.desaccessInterface }}"
          - name: SMTP_SERVER
            value: 'smtp-cx.socketlabs.com:587'
          - name: SMTP_USERNAME
            valueFrom:
              secretKeyRef:
                name: smtp
                key: username
          - name: SMTP_PASSWORD
            valueFrom:
              secretKeyRef:
                name: smtp
                key: password
          - name: DESACCESS_ADMIN_EMAILS
            value: 'desaccess-admins@lists.ncsa.illinois.edu'
          - name: DESACCESS_PUBLIC_EMAILS
            value: 'desaccess-public@lists.ncsa.illinois.edu'
          - name: DESACCESS_PRIVATE_EMAILS
            value: 'desaccess-collab@lists.ncsa.illinois.edu'
          - name: ORACLE_USER_MANAGER_DB_PUBLIC_DBS
            value: 'desdr'
          - name: ALLOWED_ROLE_LIST
            value: "{{ .Values.desaccessAllowedRoleList }}"
          - name: JIRA_DEFAULT_ASSIGNEE
            value: "{{ .Values.desaccessJiraDefaultAssignee }}"
          - name: LIMIT_CUTOUTS_CONCURRENT_JOBS
            value: "{{ .Values.limitCutoutsConcurrentJobs }}"
          - name: LIMIT_CUTOUTS_CUTOUTS_PER_JOB
            value: "{{ .Values.limitCutoutsCutoutsPerjob }}"
          - name: LIMIT_MAX_JOB_DURATION_HOURS
            value: "{{ .Values.limitMaxJobDurationHours }}"
          - name: DESACCESS_JOB_FILES_LIFETIME
            value: {{ .Values.desaccessJobFilesLifetime | quote }}
          - name: DESACCESS_JOB_FILES_WARNING_PERIOD
            value: {{ .Values.desaccessJobFilesWarningPeriod | quote }}
          - name: DESACCESS_JOB_FILES_MAX_RENEWALS
            value: "{{ .Values.desaccessJobFilesMaxRenewals }}"
          - name: MONITOR_SERVICE_ACCOUNT_USERNAME
            value: "{{ .Values.monitorAccountServiceName }}"
          - name: DESARCHIVE_HOST_PATH
            value: "{{ .Values.data.desarchive.path }}"
          - name: COADD_HOST_PATH
            value: "{{ .Values.data.coadd.path }}"
          - name: DR2_TILE_HOST_PATH
            value: "{{ .Values.data.dr2_tiles.path }}"
        volumeMounts:
        - name: db-init
          mountPath: "/config"
          # subPath: "db_init.yaml"
        - name: job-files
          mountPath: "/jobfiles"
        - name: jira-access
          mountPath: "/home/worker/jira_access.yaml"
          subPath: "jira_access.yaml"
        - name: oracle-user-manager
          mountPath: "/home/worker/oracle_user_manager.yaml"
          subPath: "oracle_user_manager.yaml"
        - name: desarchive-volume
          mountPath: '/des003/desarchive'
          readOnly: true
        - name: coadd-volume
          mountPath: '/des004/coadd'
          readOnly: true
        - name: dr1-volume
          mountPath: '/tiles/dr1'
          readOnly: true
        - name: dr2-volume
          mountPath: '/tiles/dr2'
          readOnly: true
        - name: email-file-storage
          mountPath: '/email_list'
      volumes:
      - name: db-init
        secret:
          secretName: {{ .Release.Name }}-backend-db-init
          items:
          - key: db_init
            path: "db_init.yaml"
      - name: job-files
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-tasks
      - name: jira-access
        secret:
          secretName: {{ .Release.Name }}-jira
      - name: oracle-user-manager
        secret:
          secretName: {{ .Release.Name }}-oracle-user-manager
      - name: desarchive-volume
        hostPath:
          path: {{ .Values.data.desarchive.path | quote }}
      - name: coadd-volume
        hostPath:
          path: {{ .Values.data.coadd.path | quote }}
      - name: dr1-volume
        hostPath:
          path: {{ .Values.data.dr1_tiles.path | quote }}
      - name: dr2-volume
        hostPath:
          path: {{ .Values.data.dr2_tiles.path | quote }}
      - name: email-file-storage
        persistentVolumeClaim:
          claimName: {{ .Release.Name }}-email
{{ end }}
