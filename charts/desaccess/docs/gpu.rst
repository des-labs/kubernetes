GPU Configuration
======================================================

`At this point, a working setup can be tested by running a base CUDA
container: <https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#installing-on-ubuntu-and-debian>`__

.. code:: sh


   manninga@desgpu1$ sudo docker run --rm --gpus all nvidia/cuda:10.0-base nvidia-smi
   Thu Sep 17 14:50:50 2020       
   +-----------------------------------------------------------------------------+
   | NVIDIA-SMI 410.79       Driver Version: 410.79       CUDA Version: 10.0     |
   |-------------------------------+----------------------+----------------------+
   | GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
   | Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
   |===============================+======================+======================|
   |   0  Tesla V100-PCIE...  Off  | 00000000:0B:00.0 Off |                    0 |
   | N/A   28C    P0    27W / 250W |      0MiB / 32480MiB |      1%      Default |
   +-------------------------------+----------------------+----------------------+
                                                                                  
   +-----------------------------------------------------------------------------+
   | Processes:                                                       GPU Memory |
   |  GPU       PID   Type   Process name                             Usage      |
   |=============================================================================|
   |  No running processes found                                                 |
   +-----------------------------------------------------------------------------+

Try running the diagnostic pod:

.. code:: yaml

   apiVersion: v1
   kind: Pod
   metadata:
     name: gpu-operator-test
     namespace: #@ data.values.namespace
   spec:
     nodeSelector:
       gpu: 'true'
     restartPolicy: OnFailure
     containers:
       - name: cuda-vector-add
         # https://github.com/kubernetes/kubernetes/blob/v1.7.11/test/images/nvidia-cuda/Dockerfile
         image: "k8s.gcr.io/cuda-vector-add:v0.1"
         resources:
           limits:
             nvidia.com/gpu: 1

in namespace ``gputest``:

.. code:: sh

   manninga@darkmatter$ ~/deployment_public/scripts/deploy -a gputest -n gputest -c prod -u cluster-admin 

The pod fails with this error event:

.. code:: sh

   Error: failed to start container "cuda-vector-add": Error response from daemon: OCI runtime create failed: container_linux.go:349: starting container process caused "process_linux.go:449: container init caused \"process_linux.go:432: running prestart hook 0 caused \\\"error running hook: exit status 1, stdout: , stderr: exec command: [/usr/bin/nvidia-container-cli --load-kmods configure --ldconfig=@/sbin/ldconfig --device=GPU-9fd8cc06-f8c6-e845-6af6-fd79b7b102f8 --compute --compat32 --graphics --utility --video --display --require=cuda>=8.0 --pid=31007 /var/lib/docker/devicemapper/mnt/b4fd5262b97b3b9d992ed637111c5358d9c7cdebb07e744fe00bdfb4cee11821/rootfs]\\\\nnvidia-container-cli: mount error: stat failed: /dev/nvidia-modeset: no such file or directory\\\\n\\\"\"": unknown

   Source: kubelet desgpu1.cosmology.illinois.edu
   Count: 1
   Sub-object: spec.containers{cuda-vector-add}
   Last seen: 2020-09-17T15:14:20Z

Added the environment variable override:

.. code:: yaml

   env:
   - name: "NVIDIA_DRIVER_CAPABILITIES"
       value: "compute,utility"

and the ``cuda-vector-add`` pod completed successfully!

.. code:: sh

   manninga@darkmatter$ kubectl logs -n gputest gpu-operator-test 
   [Vector addition of 50000 elements]
   Copy input data from the host memory to the CUDA device
   CUDA kernel launch with 196 blocks of 256 threads
   Copy output data from the CUDA device to the host memory
   Test PASSED
   Done

We decided to upgrade the ``desgpu1`` drivers to support CUDA Toolkit
version 11. Prior to upgrading the drivers, the versions installed were:

.. code:: sh

   manninga@desgpu1$ cat /proc/driver/nvidia/version
   NVRM version: NVIDIA UNIX x86_64 Kernel Module  410.79  Thu Nov 15 10:41:04 CST 2018
   GCC version:  gcc version 4.8.5 20150623 (Red Hat 4.8.5-36) (GCC) 


   manninga@desgpu1$ nvidia-smi
   Thu Sep 17 13:30:19 2020       
   +-----------------------------------------------------------------------------+
   | NVIDIA-SMI 410.79       Driver Version: 410.79       CUDA Version: 10.0     |
   |-------------------------------+----------------------+----------------------+
   | GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
   | Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
   |===============================+======================+======================|
   |   0  Tesla V100-PCIE...  Off  | 00000000:0B:00.0 Off |                    0 |
   | N/A   28C    P0    27W / 250W |      0MiB / 32480MiB |      0%      Default |
   +-------------------------------+----------------------+----------------------+
                                                                                  
   +-----------------------------------------------------------------------------+
   | Processes:                                                       GPU Memory |
   |  GPU       PID   Type   Process name                             Usage      |
   |=============================================================================|
   |  No running processes found                                                 |
   +-----------------------------------------------------------------------------+

https://www.nvidia.com/Download/driverResults.aspx/162617/en-us

::

   Tesla Driver for Linux RHEL 7
    
   Version:    450.51.06
   Release Date:   2020.7.28
   Operating System:   Linux 64-bit RHEL7
   CUDA Toolkit:   11.0
   Language:   English (US)
   File Size:  294.43 MB 

   Once you accept the download please follow the steps listed below

   i) `rpm -i nvidia-driver-local-repo-rhel7-450.51.06-1.0-1.x86_64.rpm'
   ii) `yum clean all`
   iii) `yum install cuda-drivers`
   iv) `reboot` 

Step #3 above fails with a conflict:

.. code:: sh

   Error: nvidia-driver-latest conflicts with 3:nvidia-driver-latest-dkms-450.51.06-1.el7.x86_64
   Error: nvidia-driver-latest-dkms conflicts with 3:nvidia-driver-branch-450-450.51.06-1.el7.x86_64
   Error: nvidia-driver-branch-450 conflicts with 3:nvidia-driver-latest-450.51.06-1.el7.x86_64
   Error: nvidia-driver-latest conflicts with 3:nvidia-driver-branch-450-450.51.06-1.el7.x86_64
   Error: nvidia-driver-branch-450 conflicts with 3:nvidia-driver-latest-dkms-450.51.06-1.el7.x86_64
   Error: nvidia-driver-latest-dkms conflicts with 3:nvidia-driver-latest-450.51.06-1.el7.x86_64

We upgraded the NVIDIA drivers on ``desgpu1`` and rebooted:

.. code:: sh

   manninga@desgpu1$ nvidia-smi
   Thu Sep 17 16:30:59 2020       
   +-----------------------------------------------------------------------------+
   | NVIDIA-SMI 450.51.06    Driver Version: 450.51.06    CUDA Version: 11.0     |
   |-------------------------------+----------------------+----------------------+
   | GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
   | Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
   |                               |                      |               MIG M. |
   |===============================+======================+======================|
   |   0  Tesla V100-PCIE...  Off  | 00000000:0B:00.0 Off |                    0 |
   | N/A   28C    P0    26W / 250W |      0MiB / 32510MiB |      0%      Default |
   |                               |                      |                  N/A |
   +-------------------------------+----------------------+----------------------+
                                                                                  
   +-----------------------------------------------------------------------------+
   | Processes:                                                                  |
   |  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
   |        ID   ID                                                   Usage      |
   |=============================================================================|
   |  No running processes found                                                 |
   +-----------------------------------------------------------------------------+

Base image for JupyterLab server is too large (over 10GB) and so image
pull fails.

.. code:: sh

   ib/docker/devicemapper/mnt/b81e3bbb29cd6ec42387d701cf9c771a10acca4bb09d14f5277a84c4f6da97ee
   /dev/dm-19                10G  2.2G  7.8G  22% /var/lib/docker/devicemapper/mnt/c310ddd3ee9368b59d8a3f1448bad0b0bf5f4c68dfcfff1b5ff20c0e12822182
   tmpfs                     16G   12K   16G   1% /var/lib/kubelet/pods/4f52427e-b4e1-492f-9d12-35df1990d9f7/volumes/kubernetes.io~secret/default-token-sd6ql
   /dev/dm-20                10G   34M   10G   1% /var/lib/docker/devicemapper/mnt/ae2d915f1070d49a58148e59f3a78721509424ce786979eed094e8c7a25f621e
   shm                       64M     0   64M   0% /var/lib/docker/containers/e0d86070f654363fab57a70609437093a08e1d9a38280da602c2f7c28eb10e93/mounts/shm
   /dev/dm-21                10G  9.9G  160M  99% /var/lib/docker/devicemapper/mnt/8b361f5d4ef25e4ea36be34719ee5b1742b51f0ffff0d9ddfbbdbf81744f336a

Tried to switch to ``overlay`` or ``overlay2`` to allow larger base
image size but RHEL7 does not support this.

.. code:: json

   {
       "default-runtime": "nvidia",
       "runtimes": {
           "nvidia": {
               "path": "/usr/bin/nvidia-container-runtime",
               "runtimeArgs": []
           }
       },
       "storage-driver": "overlay",
       "storage-opt": [ "dm.basesize=20G" ]
   }

Changed base image of JupyterLab server to
``nvidia/cuda:11.0-base-ubuntu20.04`` and after building and deploying,
I followed the instructions at
https://varhowto.com/install-pytorch-cuda-11-0 to install PyTorch with
CUDA 11.0 support from source. However when I imported ``pytorch`` the
error ``AssertionError: Torch not compiled with CUDA enabled`` was
encountered.

Another option is to try

.. code:: sh

   conda install -c pytorch torchvision cudatoolkit=11.0 pytorch

Running the pytorch Docker image demonstrated functioning GPU access
from a Docker container:

.. code:: sh

   manninga@desgpu1$ sudo docker run --gpus all --rm -ti --ipc=host pytorch/pytorch:latest python
   Python 3.7.7 (default, May  7 2020, 21:25:33) 
   [GCC 7.3.0] :: Anaconda, Inc. on linux
   Type "help", "copyright", "credits" or "license" for more information.
   >>> import torch
   >>> torch.cuda.get_device_name(0)
   'Tesla V100-PCIE-32GB'
   >>> device_id = torch.cuda.current_device()
   >>> gpu_properties = torch.cuda.get_device_properties(device_id)

   >>> print("Found %d GPUs available. Using GPU %d (%s) of compute capability %d.%d with "
   ...  "%.1fGb total memory.\n" % 
   ...  (torch.cuda.device_count(),
   ...  device_id,
   ...  gpu_properties.name,
   ...  gpu_properties.major,
   ...  gpu_properties.minor,
   ...  gpu_properties.total_memory / 1e9))
   Found 1 GPUs available. Using GPU 0 (Tesla V100-PCIE-32GB) of compute capability 7.0 with 34.1Gb total memory.

   >>> print(torch.rand(2,3).cuda())
   tensor([[0.1052, 0.0307, 0.7477],
           [0.0839, 0.4818, 0.2385]], device='cuda:0')
   >>> print(torch.rand(2,3))
   tensor([[0.6025, 0.2655, 0.3091],
           [0.2272, 0.0995, 0.3760]])

