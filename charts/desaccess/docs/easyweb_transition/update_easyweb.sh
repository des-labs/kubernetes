#!/bin/bash

export CLUSTER=$1
export NAMESPACE=$2
export SELECTOR=desaccess-frontend
if [[ "${CLUSTER}" == "prod" ]]; then
  export SELECTOR=desaccess
fi
export KUBECONFIG=$HOME/deployment_public/private/infrastructure/cluster-${CLUSTER}/.kube/config
kubectl config use-context cluster-admin@cluster-${CLUSTER}
# # Fetch the original "gold" copy of the content
# kubectl cp desaccess-7bbc95b4c8-gzt2k:easyweb/static/elements/elements-built.html ./elements-built.$(date +"%Y%m%d%H%M%S").html
# exit 0
for POD_ID in $(kubectl -n ${NAMESPACE} get pod --selector=app=${SELECTOR} -o jsonpath='{.items[*].metadata.name}'); do
    echo "Updating pod: ${POD_ID}..."
    kubectl cp "elements-built.public.html" "${POD_ID}:easyweb/static/elements/elements-built.html"
done


    # kubectl exec -i -n "${NAMESPACE}" "${POD_ID}" -- ls -l src/components/des-pages/des-home.js #easyweb/static/des_components/des-pages/des-home.html