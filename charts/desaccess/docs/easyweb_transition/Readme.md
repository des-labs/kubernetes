DESaccess Transition
======================

We are shutting down the original `/easyweb` deployment and transitioning users to the new `/desaccess` deployment. To update the easyweb deployment with a warning message, we will push updates to the live running code in the pods instead of redeploying from scratch.

A backup of the original page content is `elements-built.20201007092537.html`.

To update the DESaccess easyweb pods, run

```sh
./update_easyweb.sh prod default
```