DESDR Server
==============================================

Original server
---------------------------------------

desdr-server.ncsa.illinois.edu (141.142.161.38)

NGINX info:

```sh
$ nginx -V
nginx version: nginx/1.14.0
built by gcc 4.8.5 20150623 (Red Hat 4.8.5-16) (GCC) 
built with OpenSSL 1.0.2k-fips  26 Jan 2017
TLS SNI support enabled
configure arguments: --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib64/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic -fPIC' --with-ld-opt='-Wl,-z,relro -Wl,-z,now -pie'
```

NGINX configuration:

```conf

    # For more information on configuration, see:
    #   * Official English Documentation: http://nginx.org/en/docs/
    #   * Official Russian Documentation: http://nginx.org/ru/docs/

    user nginx;
    worker_processes auto;
    error_log /var/log/nginx/error.log;
    pid /run/nginx.pid;

    # Load dynamic modules. See /usr/share/nginx/README.dynamic.
    include /usr/share/nginx/modules/*.conf;
    load_module modules/ngx_http_fancyindex_module.so;

    events {
        worker_connections 1024;
    }

    http {
        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';

        access_log  /var/log/nginx/access.log  main;

        sendfile            on;
        tcp_nopush          on;
        tcp_nodelay         on;
        keepalive_timeout   65;
        types_hash_max_size 2048;

        include             /etc/nginx/mime.types;
        default_type        application/octet-stream;

        # Load modular configuration files from the /etc/nginx/conf.d directory.
        # See http://nginx.org/en/docs/ngx_core_module.html#include
        # for more information.
        include /etc/nginx/conf.d/*.conf;

        server {
            listen       80 default_server;
            listen       [::]:80 default_server;
            server_name  desdr-server.ncsa.illinois.edu;
            root         /usr/share/nginx/html;
            index  index.html index.htm;

      #Disable unwanted HTTP Requests
      if ($request_method !~ ^(GET|HEAD|POST)$ )
      {
            return 405;
            }
            server_tokens off;

            # Load configuration files for the default server block.
            include /etc/nginx/default.d/*.conf;

            location / {
                root     /usr/share/nginx/html;
                index  index.html index.htm;
            }

      location /despublic/ {

        root /;
                    fancyindex on;
                    fancyindex_exact_size off;
                    fancyindex_footer "/footer.html";
                    fancyindex_ignore "/footer.html";
              
            }
      location /desprivate/ {

        root /;
                    fancyindex on;
                    fancyindex_exact_size off;
                    fancyindex_footer "/footer.html";
                    fancyindex_ignore "/footer.html";
                    auth_basic "Restricted Content";
                    auth_basic_user_file /etc/nginx/.htpasswd;
              
            }
      location /workdir/ {

        root /des004/despublic_scratch/;
              
            }
      location /easyweb/ {

        root /des004/deslabs/;
              
            }
      location /jhub/ {

        root /des004/deslabs/;
              
            }
      location /internal/ {

        root /des004/despublic_scratch/;
              
            }

            error_page 404 /404.html;
                location = /404.html {
            }

            error_page 500 502 503 504 /50x.html;
                location = /50x.html {
            }
        }

    # Settings for a TLS enabled server.
    #
    #    server {
    #        listen       443 ssl http2 default_server;
    #        listen       [::]:443 ssl http2 default_server;
    #        server_name  _;
    #        root         /usr/share/nginx/html;
    #
    #        ssl_certificate "/etc/pki/nginx/server.crt";
    #        ssl_certificate_key "/etc/pki/nginx/private/server.key";
    #        ssl_session_cache shared:SSL:1m;
    #        ssl_session_timeout  10m;
    #        ssl_ciphers HIGH:!aNULL:!MD5;
    #        ssl_prefer_server_ciphers on;
    #
    #        # Load configuration files for the default server block.
    #        include /etc/nginx/default.d/*.conf;
    #
    #        location / {
    #        }
    #
    #        error_page 404 /404.html;
    #            location = /40x.html {
    #        }
    #
    #        error_page 500 502 503 504 /50x.html;
    #            location = /50x.html {
    #        }
    #    }

    }
```

Root directory contents:

```sh
lrwxrwxrwx    1 root  root    18 Aug 30  2018 desprivate -> /des004/desprivate
lrwxrwxrwx.   1 root  root    17 Dec  5  2017 despublic -> /des004/despublic
lrwxrwxrwx.   1 root  root    25 Dec  5  2017 despublic_scratch -> /des004/despublic_scratch
```
