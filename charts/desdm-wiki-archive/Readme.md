# DESDM Wiki Archive

This is a static archive of the contents of the "DESDM" Space (as of 2024/01/10) on the Jira Confluence instance originally hosted at NCSA. 

See https://github.com/nginxinc/nginx-s3-gateway/blob/master/docs/getting_started.md to learn about the S3 gateway.
