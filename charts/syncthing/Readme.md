Syncthing Device for a Persistent Peer
============================================

Overview
----------------------------

Syncthing provides a way to share arbitrary numbers of files between any number of people without the need for a central server. It is a peer-to-peer (P2P) application that automagically syncs files in the background. This deployment app launches a Syncthing "device" (Persistent Peer) that offers a high-availability peer for members of a team to include in their file shares.

For example, imagine there are three people in a team who want to share various folders with each other during the course of their work. They can accomplish this with Syncthing using only their three workstations, by exchanging Device IDs and sharing the folders. However, what if one person modifies or adds a file while the other two are offline, and then that person goes offline before the other two come online? In that event, they would not receive the update. This is where a Persistent Peer is helpful, because in this situation, it is always online to receive updates and then sync them with the other team members when they come online again.

Usage
------------------------------

Team members can add the Persistent Peer Device ID to their individual Syncthing apps on their workstations. Then they will need to login to the web GUI at `https://example.com/syncthing` and allow the Persistent Peer to accept this connection.

**Note:** When accepting their offered Device, team members can select "Introducer" and "Auto Accept" so that the Persistent Peer will automatically accept any folder they share with it.

Deployment components
----------------------------

There are two deployments. The first uses a custom-built image from a Dockerfile that essentially just downloads a particular version of Syncthing. The second deployment is an NGINX webserver for use as a reverse-proxy, allowing the web GUI to be accessible at some path of an existing domain (which already has a TLS cert) such as `https://example.com/syncthing`.

Initialization and Configuration
------------------------------------

Generate the Syncthing instance configuration file `config.xml` and identity certificates by running the Docker container locally (create a folder `config` to capture the files):

```
$ docker run -it --rm --name syncthing \
    -v "$(pwd)/config":/srv/config \
    -u syncthing \
    hub.ncsa.illinois.edu/des/syncthing:latest \
    bash

syncthing@bd919f7d54a1:/srv$ /srv/syncthing/syncthing -home=/srv/config
```

This will generate the `config.xml` and `cert.pem` and `key.pem` files you will need to upload to the persistent storage of the deployed Syncthing app.

The `config.xml` file must be customized to set the listening address to `0.0.0.0:8080` prior to starting the Syncthing pod:
```
<gui enabled="true" tls="false" debugging="false">
    <address>0.0.0.0:8080</address>
    <apikey>********</apikey>
    <theme>default</theme>
</gui>
```

Once Syncthing has been deployed and is accessible at `https://deslabs.ncsa.illinois.edu/syncthing/` (using Basic Auth for secure access), adjust the settings using the GUI to set `/srv/data` as the default directory for new shares, and disable warnings about the admin GUI being insecure and accessible without a password. The Syncthing GUI is protected at the ingress controller level using Kubernetes annotations on the Ingress defined for the app. The authentication credentials for this HTTP basic auth are stored as a standard Secret.
