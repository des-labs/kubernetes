{{- define "desiam.backendEnv" -}}
- name: URL_BASE_PATH
  value: {{ .Values.ingress.basePath }}
- name: DJANGO_HOSTNAME
  value: {{ .Values.ingress.hostname }}
- name: DJANGO_DEBUG
  value: {{ .Values.django.debug | quote }}
- name: DJANGO_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: des-iam-django-secrets
      key: django_secret_key
- name: OIDC_RSA_PRIVATE_KEY
  valueFrom:
    secretKeyRef:
      name: des-iam-django-secrets
      key: django_oidc_rsa_private_key
- name: DB_HOST
  value: {{ .Values.postgresql.hostname }}
- name: DB_USER
  valueFrom:
    secretKeyRef:
      name: des-iam-django-secrets
      key: postgresql-username
- name: DB_PASS
  valueFrom:
    secretKeyRef:
      name: des-iam-django-secrets
      key: postgresql-password
- name: DB_NAME
  valueFrom:
    secretKeyRef:
      name: des-iam-django-secrets
      key: postgresql-database
{{- end -}}
